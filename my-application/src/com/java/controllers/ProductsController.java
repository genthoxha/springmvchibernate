package com.java.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/products")
public class ProductsController {

	@RequestMapping("/showProducts")
	public String showHtml() {
		return "products";
	}
}
