package com.java.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.java.models.Product;

@Controller
@RequestMapping("/create")
public class CreateController {

/*	@RequestMapping("/createNew")
	public String showHtml() {
		return "createProduct";
	}

	@RequestMapping("/processForm")
	public String processForm() {
		return "confirmationPage";
	}

	@RequestMapping("/processFormTwo")
	public String processFormTwo(HttpServletRequest request, Model model) {

		String name = request.getParameter("productName");
		String price = request.getParameter("productPrice");
		String type = request.getParameter("productType");
		
		model.addAttribute("productData"," "+name+" : "+price+": "+type);

		return "confirmationPage";
	}*/
	
	@RequestMapping("/createNew")
	public String sendModel(Model model) {
		
		Product theProduct = new Product();
		
		model.addAttribute("product",theProduct);
		
		
		return "createProduct";
	}
	
	@RequestMapping("/theProcessFormWithForm")
	public String theProcessFormWithForm(@ModelAttribute ("product")Product product) {
		
		Product p = new Product();
		
		// log the input data
		System.out.println("theStudent: "+product.getName()+ " "+product.getPrice());
		
		return "confirmationPage";
		
	}
	

}
