package com.java.models;

import java.util.LinkedHashMap;

public class Product {

	private String id;
	private String name;
	private String price;
	private String producer;
	private LinkedHashMap<String, String> producerOptions;

	
	public String getProducer() {
		return producer;
	}

	public void setProducer(String producer) {
		this.producer = producer;
	}

	

	public Product() {
		producerOptions = new LinkedHashMap<>();
		producerOptions.put("BR", "Brazil");
		producerOptions.put("FR", "France");
		producerOptions.put("DE", "Germany");
		producerOptions.put("IN", "India");
		producerOptions.put("US", "United States of America");

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public LinkedHashMap<String, String> getProducerOptions() {
		return producerOptions;
	}

	public void setProducerOptions(LinkedHashMap<String, String> producerOptions) {
		this.producerOptions = producerOptions;
	}

	

}
