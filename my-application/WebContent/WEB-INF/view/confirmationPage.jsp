<!DOCTYPE>
<html>
<head>
<title>My products</title>

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/style.css">
</head>
</head>

<body>

	<jsp:include page="header.jsp"/>
	
	Product name: ${product.name}
	Product price: ${product.price}
	
	Producer country: ${product.producer }


</body>
</html>