<html>
<head>
<title></title>

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/style.css">
</head>

<body>

	<jsp:include page="header.jsp" />
	<div class="form-container">
		<form>
			<div class="row">
				<div class="col">
					Product name : ${product.name}
					Product type : ${product.type}
					Product producer: ${product.producer} 
				</div>
				
			</div>
		</form>

	</div>
</body>

</html>