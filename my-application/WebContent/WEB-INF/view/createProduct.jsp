<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<title></title>

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/style.css">
</head>

<body>

	<jsp:include page="header.jsp" />


	<form:form action="theProcessFormWithForm" modelAttribute="product">
		<form:input path="name" />
		<form:input path="price" />
		<input type="submit" value="Submit" />

		<%-- <form:select path="producer">
			<form:option value="Brazil" label="Brazil"></form:option>
			<form:option value="USA" label="Usa"></form:option>
			<form:option value="GERMANY" label="Germany"></form:option>
		</form:select> --%>
		
		<form:select path="producer">
		
			<!-- Call getProducerOptions from Product model  -->
			<form:options items="${product.producerOptions}" />
					
		</form:select>
		


	</form:form>


</body>

</html>