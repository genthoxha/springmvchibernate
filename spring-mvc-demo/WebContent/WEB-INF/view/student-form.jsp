<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
<head>
<title>Student Registration form</title>
</head>

<body>

	<form:form action="processForm" modelAttribute="student">
		
		First name: <form:input path="firstName" />

		<br>
		<br>
		
		Last name: <form:input path="lastName" />

		<br>
		<br>
		
		Country:
		
<%-- 		<form:select path="country">
		
				<form:option value="Brazil" label="Brazil"/>
				<form:option value="Franz" label="Franz"/>
				<form:option value="Germany" label="Germany"/>
				<form:option value="India" label="India"/>
				
				student.countryOptions will call student.getCountryOptions()
				<form:options items="${student.countryOptions}"/>
				
				
		</form:select> --%>

		<!-- Get countries from properties file -->
		<form:select path="country">
			<form:options items="${theCountryOptions}" />
		</form:select>

		<br><br>
		
		Favorite Langugage: 
		
		<%-- Java <form:radiobutton path="favoriteLanguage" value="Java"/>
		C# <form:radiobutton path="favoriteLanguage" value="C#"/>
		PHP <form:radiobutton path="favoriteLanguage" value="PHP"/>
		RUBY <form:radiobutton path="favoriteLanguage" value="Ruby"/> --%>
		
		
		<!-- How to populate radiobuttons with items from Java class? -->
		<form:radiobuttons path="favoriteLanguage" items="${student.favoriteLanguageOptions}"  />
		
		<br><br>
		
		Operating Systems:
		
		Linux <form:checkbox path="operatingSystems" value="Linux" />
		Mac OS <form:checkbox path="operatingSystems" value="Mac OS" />
		MS Windows <form:checkbox path="operatingSystems"  value="MS Windows"/>

		<input type="submit" value="Submit" />

	</form:form>



</body>





</html>